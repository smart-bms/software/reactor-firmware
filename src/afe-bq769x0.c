/*
 * Copyright (c) 2020 Reactor Energy
 * Mieszko Mazurek <mimaz@gmx.com>
 */

#include "afe.h"
#include "bq769x0.h"

#include <zephyr.h>

#define PROT_FLAGS (BQ769X0_OCD | BQ769X0_SCD | \
		    BQ769X0_OV | BQ769X0_UV)

static int
activate (struct afe *afe)
{
    int ret;

    ret = bq769x0_configure ("I2C_1", 0x18, "GPIOB", 5);

    if (ret < 0) {
	return ret;
    }

    bq769x0_set_delay (BQ769X0_SCD_DELAY_400US,
		       BQ769X0_OCD_DELAY_1280MS,
		       BQ769X0_OV_DELAY_1S,
		       BQ769X0_UV_DELAY_1S);
    bq769x0_set_scd (BQ769X0_SCD_THRESH_100MV);
    bq769x0_set_ocd (BQ769X0_OCD_THRESH_50MV);
    bq769x0_set_ov (3800);
    bq769x0_set_uv (0);
    bq769x0_enable (BQ769X0_CHG_ON | BQ769X0_DSG_ON);

    return 0;
}

static int
deactivate (struct afe *afe)
{
    return 0;
}

static int
get_cell_count (struct afe *afe)
{
    return bq769x0_cell_count ();
}

static int
get_voltage (struct afe *afe, int cellnum)
{
    uint32_t value;

    bq769x0_voltage (cellnum, &value);

    return value;
}

static int
get_current (struct afe *afe)
{
    int32_t value;

    bq769x0_current (&value);

    return value;
}

static int
get_protection (struct afe *afe)
{
    uint32_t stat;
    int prot;

    bq769x0_status (&stat);
    bq769x0_write (0, (const char *) &stat, 1);

    prot = 0;

    if (stat & (BQ769X0_OCD | BQ769X0_SCD)) {
	if (afe_get_current (afe) > 0) {
	    prot |= AFE_PROT_CHG_OCP;
	} else {
	    prot |= AFE_PROT_DSG_OCP;
	}
    }

    if (stat & BQ769X0_OV) {
	prot |= AFE_PROT_CELL_OVP;
    }

    if (stat & BQ769X0_UV) {
	prot |= AFE_PROT_CELL_UVP;
    }

    return prot;
}

static int
get_range (struct afe *afe, int what, int *low, int *high)
{
    switch (what) {
    case AFE_PROT_CELL_OVP:
    case AFE_PROT_CELL_UVP:
	*low = 1000;
	*high = 10000;
	return 0;

    case AFE_PROT_CHG_OCP:
    case AFE_PROT_DSG_OCP:
	*low = 1000;
	*high = 100000;
	return 0;

    case AFE_PROT_OTP:
    case AFE_PROT_UTP:
	*low = -100;
	*high = 100;
	return 0;

    default:
	return -1;
    }
}

static int
get_temperatures (struct afe *afe, int *values, int count)
{
    return bq769x0_temperature (values, count);
}

static int
get_status (struct afe *afe)
{
    uint32_t bqflags;
    int flags;

    flags = 0;

    bq769x0_status (&bqflags);

    if (bqflags & BQ769X0_CHG_ON) {
	flags |= AFE_STAT_CHARGING;
    }

    if (bqflags & BQ769X0_DSG_ON) {
	flags |= AFE_STAT_DISCHARGING;
    }

    if (bqflags & PROT_FLAGS) {
	flags |= AFE_STAT_PROTECTED;
    }

    return flags;
}

static int
enable (struct afe *afe, int status)
{
    int flags;

    flags = 0;

    if (status & AFE_STAT_CHARGING) {
	flags |= BQ769X0_CHG_ON;
    }

    if (status & AFE_STAT_DISCHARGING) {
	flags |= BQ769X0_DSG_ON;
    }

    if (status & AFE_STAT_CURRENT) {
	flags |= BQ769X0_CC_EN;
    }

    if (status & (AFE_STAT_VOLTAGE | AFE_STAT_TEMPERATURE)) {
	flags |= BQ769X0_ADC_EN;
    }

    bq769x0_enable (flags);

    return 0;
}

static int
disable (struct afe *afe, int status)
{
    int flags;

    flags = 0;

    if (status & AFE_STAT_CHARGING) {
	flags |= BQ769X0_CHG_ON;
    }

    if (status & AFE_STAT_DISCHARGING) {
	flags |= BQ769X0_DSG_ON;
    }

    if (status & AFE_STAT_CURRENT) {
	flags |= BQ769X0_CC_EN;
    }

    if (status & (AFE_STAT_VOLTAGE | AFE_STAT_TEMPERATURE)) {
	flags |= BQ769X0_ADC_EN;
    }

    if (status & AFE_STAT_PROTECTED) {
	flags |= PROT_FLAGS;
    }

    bq769x0_disable (flags);

    return 0;
}

static int
get_error (struct afe *afe)
{
    return 0;
}

static int
read_registers (struct afe *afe, int addr, void *data, int bytes)
{
    return bq769x0_read (addr, data, bytes);
}

static int
write_registers (struct afe *afe, int addr, const void *data, int bytes)
{
    return bq769x0_write (addr, data, bytes);
}

static int
get_balancing (struct afe *afe)
{
    return bq769x0_get_balancing ();
}

static int
balance_cells (struct afe *afe, int count, const int *indices)
{
    int i, flags, group_begin, group_end, lo_index, hi_index;

    flags = 0;

    for (i = 0; i < count; i++) {
	if (indices[i] < 15) {
	    group_begin = indices[i] / 5 * 5;
	    group_end = group_begin + 5;
	    lo_index = indices[i] - 1;
	    hi_index = indices[i] + 1;

	    if (lo_index >= group_begin && (flags & (1 << lo_index))) {
		continue;
	    }

	    if (hi_index < group_end && (flags & (1 << hi_index))) {
		continue;
	    }

	    flags |= 1 << indices[i];
	}
    }

    return bq769x0_set_balancing (flags);
}

static struct afe_api api = {
    .activate = activate,
    .deactivate = deactivate,
    .get_cell_count = get_cell_count,
    .get_voltage = get_voltage,
    .get_current = get_current,
    .get_protection = get_protection,
    .get_range = get_range,
    .get_temperatures = get_temperatures,
    .get_status = get_status,
    .enable = enable,
    .disable = disable,
    .get_error = get_error,
    .read_registers = read_registers,
    .write_registers = write_registers,
    .get_balancing = get_balancing,
    .balance_cells = balance_cells,
};

AFE_DEFINE (bq769x0, api, 0);
