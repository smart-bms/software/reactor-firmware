/*
 * Copyright (c) 2021 Reactor Energy
 * Mieszko Mazurek <mimaz@gmx.com>
 */

#include "current.h"
#include "conf.h"

#include <afe.h>

static int64_t charging_release_time;
static int64_t charging_lock_time;
static int64_t discharging_release_time;
static int64_t discharging_lock_time;
static uint8_t allow_charging;
static uint8_t allow_discharging;

void
current_update ()
{
    int64_t time;
    int current;

    time = k_uptime_get ();
    current = afe_get_current (NULL);

    if (current > prot_conf->chg_ocp_hard * 100) {
	charging_release_time = time + 10000;
    } else if (current > prot_conf->chg_ocp_soft * 100) {
	if (charging_lock_time == 0) {
	    charging_lock_time = time + prot_conf->chg_ocp_soft_d;
	} else if (time > charging_lock_time) {
	    charging_lock_time = 0;
	    charging_release_time = time + 10000;
	}
    } else {
	charging_lock_time = 0;
    }

    if (current < -prot_conf->dsg_ocp_hard * 100) {
	discharging_release_time = time + 10000;
    } else if (current < -prot_conf->dsg_ocp_soft * 100) {
	if (discharging_lock_time == 0) {
	    discharging_lock_time = time + prot_conf->dsg_ocp_soft_d;
	} else if (time > discharging_lock_time) {
	    discharging_lock_time = 0;
	    discharging_release_time = time + 10000;
	}
    } else {
	discharging_lock_time = 0;
    }

    allow_charging = time > charging_release_time;
    allow_discharging = time > discharging_release_time;
}

int
current_allow_charging ()
{
    return allow_charging;
}

int
current_allow_discharging ()
{
    return allow_discharging;
}
