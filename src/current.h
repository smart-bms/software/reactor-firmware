/*
 * Copyright (c) 2021 Reactor Energy
 * Mieszko Mazurek <mimaz@gmx.com>
 */

#pragma once

void	current_update		    (void);
int	current_allow_charging	    (void);
int	current_allow_discharging   (void);
