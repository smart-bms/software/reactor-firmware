/*
 * Copyright (c) 2021 Reactor Energy
 * Mieszko Mazurek <mimaz@gmx.com>
 */

int nv_size	(void);
int nv_write	(int address, const void *mem, int len);
int nv_read	(int address, void *mem, int len);
