/*
 * Copyright (c) 2021 Reactor Energy
 * Mieszko Mazurek <mimaz@gmx.com>
 */

#pragma once

#include <bcom.h>
#include <kernel.h>

#define CONF_NOTIFY(name, conf_id) \
    static void name (void); \
    STRUCT_SECTION_ITERABLE (conf_notify, __##name##_notify) = { \
	.func = name, \
	.id = conf_id, \
    }; \
    static void name (void)

struct conf_notify
{
    void (*func) ();
    int id;
};

extern const struct bcom_prot_conf *const prot_conf;
extern const struct bcom_bat_conf *const bat_conf;

void	conf_init	(void);
int	conf_use_prot	(const struct bcom_prot_conf *conf);
int	conf_use_bat	(const struct bcom_bat_conf *conf);
int	conf_write_prot	(void);
int	conf_write_bat	(void);
int	conf_load_prot	(void);
int	conf_load_bat	(void);
