/*
 * Copyright (c) 2021 Reactor Energy
 * Mieszko Mazurek <mimaz@gmx.com>
 */

#pragma once

void	temp_update		(void);
int	temp_allow_charging	(void);
int	temp_allow_discharging	(void);
