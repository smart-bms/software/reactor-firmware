/*
 * Copyright (c) 2020 Reactor Energy
 * 2020-2022 Mieszko Mazurek <mimaz@gmx.com>
 */

#include "com.h"
#include "led.h"

#include <console/tty.h>
#include <drivers/uart.h>
#include <device.h>
#include <zephyr.h>

#define QUEUE_SIZE 256

struct tty_com
{
    struct bcom_port	    port;
    struct rcom_serial	    serial;
    const struct device	   *uart;
    uint8_t		    in_queue[QUEUE_SIZE];
    uint8_t		    out_queue[QUEUE_SIZE];
    volatile int	    in_head;
    volatile int	    in_tail;
    volatile int	    out_head;
    volatile int	    out_tail;
};

static int
com_write (unsigned bytes,
	   const void *data,
	   void *closure)
{
    struct tty_com *com;
    const uint8_t *iter, *end;
    unsigned int lock_key;

    com = closure;
    iter = data;
    end = iter + bytes;

    lock_key = irq_lock ();

    while (iter != end) {
	com->out_queue[com->out_head++] = *iter++;

	if (com->out_head >= QUEUE_SIZE) {
	    com->out_head = 0;
	}

	if (com->out_head == com->out_tail) {
	    com->out_tail++;

	    if (com->out_tail >= QUEUE_SIZE) {
		com->out_tail = 0;
	    }
	}
    }

    uart_irq_tx_enable (com->uart);
    irq_unlock (lock_key);

    return bytes;
}

static struct tty_com com_array[2];
static int com_count;
static struct k_sem com_sem;

static void com_entry (void *p1, void *p2, void *p3);

K_THREAD_DEFINE (com_thread, 1024, com_entry, NULL, NULL, NULL, 0, 0, 0);

static void
com_entry (void *p1, void *p2, void *p3)
{
    struct tty_com *com;
    uint8_t byte;
    int i;

    k_sem_init (&com_sem, 0, 2);

    while (1) {
	k_sem_take (&com_sem, K_FOREVER);

	for (i = 0; i < com_count; i++) {
	    com = &com_array[i];

	    while (com->in_tail != com->in_head) {
		byte = com->in_queue[com->in_tail++];

		if (com->in_tail >= QUEUE_SIZE) {
		    com->in_tail = 0;
		}

		/*bcom_message ("r");*/
		rcom_port_feed (&com->port.base, 1, &byte);
		/*rcom_serial_feed ((struct rcom_serial *) com, &byte, 1);*/
	    }
	}
    }
}

static void
irq_callback (const struct device *uart, void *user_data)
{
    struct tty_com *com;
    uint8_t byte;

    com = user_data;

    uart_irq_update (com->uart);

    if (uart_irq_rx_ready (com->uart)) {
	uart_fifo_read (com->uart, &byte, 1);

	com->in_queue[com->in_head++] = byte;

	if (com->in_head >= QUEUE_SIZE) {
	    com->in_head = 0;
	}

	if (com->in_head == com->in_tail) {
	    com->in_tail++;

	    if (com->in_tail >= QUEUE_SIZE) {
		com->in_tail = 0;
	    }
	}

	k_sem_give (&com_sem);
    }

    if (uart_irq_tx_ready (com->uart)) {
	if (com->out_tail != com->out_head) {
	    byte = com->out_queue[com->out_tail++];

	    if (com->out_tail >= QUEUE_SIZE) {
		com->out_tail = 0;
	    }

	    uart_fifo_fill (com->uart, &byte, 1);
	} else {
	    uart_irq_tx_disable (com->uart);
	}
    }
}

extern struct bcom_receive com_recv;

struct bcom_port *
com_init_uart (const char *uart_name)
{
    struct tty_com *com;

    com = &com_array[com_count];
    com->uart = device_get_binding (uart_name);

    if (com->uart == NULL) {
	return NULL;
    }

    bcom_port_init (&com->port, &com_recv, com_write, com);
    rcom_serial_init (&com->serial, &com->port.base);

    uart_irq_callback_user_data_set (com->uart, irq_callback, com);
    uart_irq_rx_enable (com->uart);

    com_count++;

    return &com->port;
}
