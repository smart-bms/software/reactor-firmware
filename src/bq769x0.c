/*
 * Copyright (c) 2020 Reactor Energy
 * Mieszko Mazurek <mimaz@gmx.com>
 */

#include "bq769x0.h"

#include <string.h>
#include <device.h>
#include <bcom.h>
#include <drivers/i2c.h>
#include <drivers/gpio.h>
#include <conf.h>

struct be16
{
    uint8_t hi;
    uint8_t lo;
} __attribute__((packed));

struct memory_map_ctrl
{
    uint8_t cellbal[3];
    uint8_t sys_ctrl1;
    uint8_t sys_ctrl2;
    uint8_t protect1;
    uint8_t protect2;
    uint8_t protect3;
    uint8_t ov_trip;
    uint8_t uv_trip;
    uint8_t cc_cfg;
} __attribute__((packed));

struct memory_map
{
    uint8_t		    sys_stat;
    struct memory_map_ctrl  ctrl;
    struct be16		    vc[15];
    struct be16		    bat;
    struct be16		    ts[3];
    struct be16		    cc;
} __attribute__((packed));

#define CTRL_OFFSET offsetof (struct memory_map, ctrl)

_Static_assert (sizeof (struct memory_map_ctrl) == 0x0c - CTRL_OFFSET);

static uint16_t
be16_value (struct be16 b)
{
    return b.lo | ((uint16_t) b.hi << 8);
}

K_MUTEX_DEFINE (i2c_lock);
K_MUTEX_DEFINE (ctrl_lock);

struct bq
{
    const struct device	   *gpio_device;
    int			    gpio_pin;
    const struct device	   *i2c_device;
    int			    i2c_address;
    struct memory_map	    memmap;
    uint8_t		    adcgain1;
    uint8_t		    adcgain2;
    uint8_t		    adcoffset;
    struct memory_map_ctrl  ctrlmap;
    uint8_t		    statclr;
    uint8_t		    cell_map[15];
    uint16_t		    cell_voltages[15];
    uint32_t		    battery_voltage;
    uint8_t		    cell_count;
    int32_t		    adc_gain;
    int32_t		    adc_offset;
};

static struct bq glob;
static struct bq *instance = &glob;

int
bq769x0_configure (const char *i2c_name, int address,
		   const char *gpio_name, int pin)
{
    const struct device *i2c;
    uint8_t cfg;
    int ret;

    instance->gpio_device = device_get_binding (gpio_name);
    instance->gpio_pin = pin;

    gpio_pin_configure (instance->gpio_device, instance->gpio_pin,
			GPIO_OUTPUT_ACTIVE);
    gpio_pin_set (instance->gpio_device, instance->gpio_pin, 1);

    instance->i2c_device = NULL;
    instance->i2c_address = 0;

    i2c = device_get_binding (i2c_name);
    printk ("configure i2c %p\n", i2c);

    if (i2c == NULL) {
	return 0;
    }

    cfg = 0;

    ret = i2c_reg_write_byte (i2c, address, 0x0b, 0x19);
    ret |= i2c_reg_read_byte (i2c, address, 0x0b, &cfg);

    if (ret != 0) {
	return ret;
    }

    if (cfg != 0x19) {
	return -1;
    }

    i2c_reg_write_byte (i2c, address, 1, 0);
    i2c_reg_write_byte (i2c, address, 2, 0);
    i2c_reg_write_byte (i2c, address, 3, 0);

    i2c_reg_read_byte (i2c, address, 0x50, &instance->adcgain1);
    i2c_reg_read_byte (i2c, address, 0x59, &instance->adcgain2);
    i2c_reg_read_byte (i2c, address, 0x51, &instance->adcoffset);

    instance->adc_gain = 365 + (((instance->adcgain1 & 0xc) << 1) |
				(instance->adcgain2 >> 5));
    instance->adc_offset = ((int8_t) instance->adcoffset) * 1000;

    printk ("bq769x0 configured!\n");

    instance->i2c_device = i2c;
    instance->i2c_address = address;

    printk ("gpio: %s %p\n", gpio_name, instance->gpio_device);

    gpio_pin_set (instance->gpio_device, instance->gpio_pin, 0);

    return 0;
}

int
bq769x0_cell_count ()
{
    return instance->cell_count;
}

int
bq769x0_voltage (int cellnum, uint32_t *value)
{
    if (cellnum < 0) {
	*value = instance->battery_voltage;
	return 0;
    } else if (cellnum < instance->cell_count) {
	*value = instance->cell_voltages[cellnum];
	return 0;
    }

    return -1;
}

static uint32_t
calculate_voltage (uint32_t raw)
{
    return (raw * instance->adc_gain + instance->adc_offset) / 1000;
}

static uint32_t
calculate_trip (int32_t value, int32_t min, int32_t max)
{
    int32_t trip;

    if (instance->adc_gain > 0) {
	trip = (value * 1000 - instance->adc_offset) / instance->adc_gain;
    } else {
	trip = 0;
    }

    if (min > trip) {
	trip = min;
    } 

    if (max < trip) {
	trip = max;
    }

    return trip;
}

int
bq769x0_current (int32_t *value)
{
    uint16_t uraw;
    int16_t sraw;

    uraw = be16_value (instance->memmap.cc);
    sraw = *(int16_t *) &uraw;

    *value = sraw * 844 / 100;

    return 0;
}

static int
calculate_temperature(uint16_t raw)
{
	int16_t sraw;
	sraw = *(int16_t *) &raw;
	uint32_t RTS;
	uint32_t VTSX;
	/*uint32_t temp;*/

	if(instance->memmap.ctrl.sys_ctrl1 & 8){

	    //VTSX= (ADCin Decimal)x 382 μV/LSB
            //RTS= (10,000× VTSX)÷ (3.3 – VTSX)
	    VTSX = sraw*382;	
            RTS = (10*VTSX/(3300- (VTSX/1000)));

	    /*return rcom_curve_compute(&board_config->temp_curve[0], RTS);*/

	 /* if(sraw>=100 && sraw<500)
	    {
	         temp = (-11874*RTS+16354522)/10000;
	    }
       	    else if(sraw>=500 && sraw<1000)
	    {
	         temp = (-2916*RTS+11146404)/10000;
	    }
 	    else if(sraw>=1000 && sraw<2000)
	    {
	         temp = (-1200*RTS+8715473)/10000;
	    }		
	    else if(sraw>=2000 && sraw<3000)
	    {
	         temp = (-560*RTS+6790913)/10000;
	    }
	    else if(sraw>=3000 && sraw<4000)
	    {
	         temp = (-311*RTS+5449621)/10000;
	    }
	    else if(sraw>=4000 && sraw<5000)
	    {
	         temp = (-182*RTS+4317392)/10000;
	    }
	    else if(sraw>=5000 && sraw<6000)
	    {
	         temp = (-105*RTS+3232977)/10000;
	    }
	    else if(sraw>=6000 && sraw<7000)
	    {
	         temp = (-55*RTS+2051258)/10000;
	    }
	    else if(sraw>=7000 && sraw<8000)
	    {
	         temp = (-21*RTS+441144)/10000;
	    }	
	    else
	    {
 		temp = 0;
	    }

	    rcom_message_format (NULL,  "sraw=%d VTS=%dmV RTS=%d,%dkOhm",
				 (int)sraw, ((int)VTSX)/1000, (int)RTS/1000,
				(int)RTS-((int)RTS/1000*1000));	 
	    return temp;

	    */
	}
	else
	{
	    //V25= 1.200V (nominal)
	    //VTSX= (ADCin Decimal)x 382 μV/LSB
	    //TEMPDIE= 25° – ((VTSX– V25) ÷ 0.0042

	    return  250-((((int64_t)sraw*382-1200000)*10000)/42)/100000;
	}
	return 0;
}


int
bq769x0_temperature(int32_t *values, int count)
{
    uint16_t raw;
    int i;
    uint8_t sensors_nb = 0;

    /* rcom_message_format (NULL, "---"); */
    for (i = 0; i < 3; i++) {
	raw = be16_value (instance->memmap.ts[i]);

	if (raw < 8000) {
	    values[sensors_nb++] = calculate_temperature(be16_value (instance->memmap.ts[i]));
	    if(sensors_nb >= count){
		break;
 	   }
	}
    }
	return sensors_nb;
	
}

int
bq769x0_status (uint32_t *status)
{
    *status = instance->memmap.sys_stat |
	((uint32_t) instance->memmap.ctrl.sys_ctrl1 << 8) |
	((uint32_t) instance->memmap.ctrl.sys_ctrl2 << 16);

    return 0;
}

int
bq769x0_enable (uint32_t flags)
{
    k_mutex_lock (&ctrl_lock, K_FOREVER);

    instance->ctrlmap.sys_ctrl1 |= (flags >> 8) & 0xff;
    instance->ctrlmap.sys_ctrl2 |= (flags >> 16) & 0xff;

    k_mutex_unlock (&ctrl_lock);

    return 0;
}

int
bq769x0_disable (uint32_t flags)
{
    k_mutex_lock (&ctrl_lock, K_FOREVER);

    instance->statclr |= flags & 0xff;
    instance->ctrlmap.sys_ctrl1 &= ~((flags >> 8) & 0xff);
    instance->ctrlmap.sys_ctrl2 &= ~((flags >> 16) & 0xff);

    k_mutex_unlock (&ctrl_lock);

    return 0;
}

static void
begin_transfer ()
{
    k_mutex_lock (&i2c_lock, K_FOREVER);
    gpio_pin_set (instance->gpio_device, instance->gpio_pin, 1);
}

static void
end_transfer ()
{
    gpio_pin_set (instance->gpio_device, instance->gpio_pin, 0);
    k_mutex_unlock (&i2c_lock);
}

int
bq769x0_write (uint8_t addr, const uint8_t *data, uint8_t count)
{
    k_mutex_lock (&ctrl_lock, K_FOREVER);

    if (addr == 0) {
	addr++;
	instance->statclr |= *data++;
	count--;
    }

    addr--;

    if (count > sizeof (instance->ctrlmap) - addr) {
	count = sizeof (instance->ctrlmap) - addr;
    }

    if (count > 0) {
	memcpy ((uint8_t *) &instance->ctrlmap + addr, data, count);
    }

    k_mutex_unlock (&ctrl_lock);

    return 0;
}

int
bq769x0_read (uint8_t addr, uint8_t *data, uint8_t count)
{
    const uint8_t *mem;
    uint8_t buff[2];
    int end;

    if (addr < sizeof (struct memory_map)) {
	mem = (const uint8_t *) &instance->memmap + addr;
	end = sizeof (struct memory_map);
    } else if (addr >= 0x50 && addr < 0x52) {
	buff[0] = instance->adcgain1;
	buff[1] = instance->adcoffset;
	mem = buff;
	end = 0x52;
    } else if (addr >= 0x59 && addr < 0x5a) {
	buff[0] = instance->adcgain2;
	mem = buff;
	end = 0x5a;
    } else {
	return 0;
    }

    if (count > end - addr) {
	count = end - addr;
    }

    memcpy (data, mem, count);

    return count;
}

static int
ctrl_protect_offset (int offset, int set, int *get, int shift, int bits)
{
    const uint8_t *rd;
    uint8_t *wr;
    int mask;

    mask = (1 << bits) - 1;
    rd = (const uint8_t *) &instance->memmap.ctrl + offset;
    wr = (uint8_t *) &instance->ctrlmap + offset;

    if (set >= 0) {
	*wr &= ~(mask << shift);
	*wr |= (set & mask) << shift;
    }

    if (get != NULL) {
	*get = (*rd >> shift) & mask;
    }

    return 0;
}

#define ctrl_protect(reg, set, get, shift, bits) \
    ctrl_protect_offset (offsetof (struct memory_map_ctrl, reg), \
			 set, get, shift, bits)

int
bq769x0_set_delay (int scd, int ocd, int ov, int uv)
{
    k_mutex_lock (&ctrl_lock, K_FOREVER);

    ctrl_protect (protect1, scd, NULL, 3, 2);
    ctrl_protect (protect2, ocd, NULL, 4, 3);
    ctrl_protect (protect3, ov, NULL, 4, 2);
    ctrl_protect (protect3, uv, NULL, 6, 2);

    k_mutex_unlock (&ctrl_lock);

    return 0;
}

int
bq769x0_set_scd (int thresh)
{
    k_mutex_lock (&ctrl_lock, K_FOREVER);

    ctrl_protect (protect1, thresh, NULL, 0, 3);

    k_mutex_unlock (&ctrl_lock);

    return 0;
}

int
bq769x0_get_scd (int *thresh)
{
    ctrl_protect (protect1, -1, thresh, 0, 3);

    return 0;
}

int
bq769x0_set_ocd (int thresh)
{
    k_mutex_lock (&ctrl_lock, K_FOREVER);

    ctrl_protect (protect2, thresh, NULL, 0, 4);

    k_mutex_unlock (&ctrl_lock);

    return 0;
}

int
bq769x0_get_ocd (int *thresh)
{
    ctrl_protect (protect2, -1, thresh, 0, 4);

    return 0;
}

int
bq769x0_set_ov (int value)
{
    k_mutex_lock (&ctrl_lock, K_FOREVER);

    // Datasheet BQ769x  page 25
    // Over voltage
    //     value 0bx10 XXXX XXXX 1000
    // min value -1bx10 0000 0000 1000 dec 8200
    // max value 0bx10 1111 1111 1000 dec 12280

    ctrl_protect (ov_trip, calculate_trip (value,8200,12280)>>4, NULL, 0, 8);
    /* ctrl_protect (ov_trip, 255, NULL, 0, 8); */

    k_mutex_unlock (&ctrl_lock);

    return 0;
}

int
bq769x0_get_ov (int *value)
{
    *value = calculate_voltage ((1 << 13) | (instance->memmap.ctrl.ov_trip << 4) | (1 << 3));

    return 0;
}

int
bq769x0_set_uv (int value)
{
    k_mutex_lock (&ctrl_lock, K_FOREVER);

    // Datasheet BQ769x  page 25
    // Under voltage
    //     value 0bx01 XXXX XXXX 0000 
    // min value 0bx01 0000 0000 0000 dec 4096
    // max value 0bx01 1111 1111 0000 dec 8176

    ctrl_protect (uv_trip, calculate_trip (value,4096,8176)>>4, NULL, 0, 8);

    k_mutex_unlock (&ctrl_lock);

    return 0;
}

int
bq769x0_get_uv (int *value)
{
    *value = calculate_voltage ((1 << 12) | (instance->memmap.ctrl.uv_trip << 4));

    return 0;
}

int
bq769x0_set_balancing (int flags)
{
    uint8_t cellbal[3];
    int i, mapped;

    memset (cellbal, 0, 3);

    k_mutex_lock (&ctrl_lock, K_FOREVER);
    for(i = 0; i < instance->cell_count; i++) {	
    	if (flags & (1 << i)) {
	    mapped = instance->cell_map[i];

	    cellbal[mapped / 5] |= 1 << (mapped % 5);
	}
    }

    k_mutex_unlock (&ctrl_lock);

    return bq769x0_write (1, cellbal, 3);
}

int
bq769x0_get_balancing ()
{
    uint8_t cellbal[3];
    int i, flags, mapped;

    if (bq769x0_read (1, cellbal, 3) < 0) {
	return -1;
    }

    flags = 0;

    for (i = 0; i < instance->cell_count; i++) {
	mapped = instance->cell_map[i];

	if (cellbal[mapped / 5] & (1 << (mapped % 5))) {
	    flags |= 1 << i;
	}
    }

    return flags;
}

static int
clear_stat ()
{
    if (instance->statclr) {
	i2c_reg_write_byte (instance->i2c_device, instance->i2c_address,
			    0, instance->statclr);
	instance->statclr = 0;
    }

    return 0;
}

static int
write_ctrlmap ()
{
    const uint8_t *src, *end;
    uint8_t *dst;
    int ret, addr;

    src = (const uint8_t *) &instance->ctrlmap;
    end = (const uint8_t *) &instance->ctrlmap + sizeof (instance->ctrlmap);
    dst = (uint8_t *) &instance->memmap.ctrl;

    while (src < end) {
	if (*src != *dst) {
	    addr = dst - (const uint8_t *) &instance->memmap;
	    ret = i2c_reg_write_byte (instance->i2c_device,
				      instance->i2c_address,
				      addr, *src);

	    if (ret < 0) {
		return ret;
	    }

	    *dst = *src;
	}

	src++;
	dst++;
    }

    return 0;
}

static int
load_memmap ()
{
    static uint8_t start_addr = 0;

    return i2c_write_read (instance->i2c_device, instance->i2c_address,
			   &start_addr, 1,
			   &instance->memmap, sizeof (instance->memmap));
}

static void
update_voltages ()
{
    uint32_t battery;
    uint16_t value;
    int i, count;

    count = 0;
    battery = 0;

    /* rcom_message_format (NULL, "---"); */
    for (i = 0; i < 15; i++) {
	value = be16_value (instance->memmap.vc[i]) & 0x3fff;
	value = calculate_voltage (value);

	/* rcom_message_format (NULL, "vc %d: %d", i, value); */

	if (value > 200) {
	    instance->cell_map[count] = i;
	    instance->cell_voltages[count++] = value;
	    battery += value;
	}
    }

    instance->cell_count = count;
    instance->battery_voltage = battery;
}

static void
thread_entry (void *p1, void *p2, void *p3)
{
    while (1) {
	k_msleep (500);

	if (instance->i2c_device == NULL) {
	    continue;
	}

	begin_transfer ();
	clear_stat ();
	write_ctrlmap ();
	load_memmap ();

	/* i2c_reg_write_byte (instance->i2c_device, instance->i2c_address, 0, 0xff); */
	/* i2c_reg_write_byte (instance->i2c_device, instance->i2c_address, 0x05, 0x43); */

	end_transfer ();

	k_mutex_lock (&ctrl_lock, K_FOREVER);
	memcpy (&instance->ctrlmap, &instance->memmap.ctrl,
		sizeof (instance->ctrlmap));
	k_mutex_unlock (&ctrl_lock);

	update_voltages ();
    }
}

K_THREAD_DEFINE (bq769x0_thread, 1024, thread_entry,
		 NULL, NULL, NULL, 0, 0, 0);
