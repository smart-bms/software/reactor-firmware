/*
 * Copyright (c) 2020 Reactor Energy
 * 2020-2022 Mieszko Mazurek <mimaz@gmx.com>
 */

#include "com.h"
#include "state.h"
#include "afe.h"

static void
recv_ping (struct bcom_port *port, uint32_t ack)
{
    if (ack > 0) {
	bcom_port_ping (port, ack - 1);
    }
}

static void
recv_query (struct bcom_port *port, uint32_t query)
{
    uint16_t values[16];
    uint8_t count;
    int i;

    if (query & BCOM_QUERY_VOLTAGE) {
	afe_get_cell_count (NULL, &count);

	for (i = 0; i < count; i++) {
	    afe_get_voltage(NULL, i, &values[i]);
	}

	bcom_port_voltage (port, count, values);
    }

    if (query & BCOM_QUERY_STATUS) {
	bcom_port_status (port, state_get ());
    }
}

struct bcom_receive com_recv = {
    .ping = recv_ping,
    .query = recv_query,
};
