/*
 * Copyright (c) 2021 Reactor Energy
 * Mieszko Mazurek <mimaz@gmx.com>
 */

#include "nvmem.h"

#define EEPROM_DEVICE "EEPROM_0"

#ifdef EEPROM_DEVICE
#include <zephyr.h>
#include <drivers/eeprom.h>

static const struct device *
eeprom_dev ()
{
    static const struct device *dev;

    if (dev == NULL) {
	dev = device_get_binding (EEPROM_DEVICE);
    }

    return dev;
}

#else
static unsigned char fake_nvmem[4096];
#endif
#include <string.h>

int
nv_size ()
{
#ifdef EEPROM_DEVICE
    return eeprom_get_size (eeprom_dev ());
#else
    return sizeof (fake_nvmem);
#endif
}

int
nv_write (int address, const void *mem, int len)
{
#ifdef EEPROM_DEVICE
    return eeprom_write (eeprom_dev (), address, mem, len);
#else
    int maxlen;

    maxlen = sizeof (fake_nvmem) - len - address;

    if (len > maxlen) {
	len = maxlen;
    }

    if (len < 0) {
	return -1;
    }

    memcpy (fake_nvmem + address, mem, len);

    return len;
#endif
}

int
nv_read (int address, void *mem, int len)
{
#ifdef EEPROM_DEVICE
    return eeprom_read (eeprom_dev (), address, mem, len);
#else
    int maxlen;

    maxlen = sizeof (fake_nvmem) - len - address;

    if (len > maxlen) {
	len = maxlen;
    }

    if (len < 0) {
	return -1;
    }

    memcpy (mem, fake_nvmem + address, len);

    return len;
#endif
}
