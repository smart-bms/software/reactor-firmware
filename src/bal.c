/*
 * Copyright (c) 2021 Reactor Energy
 * Mieszko Mazurek <mimaz@gmx.com>
 */

#include "bal.h"
#include "conf.h"
#include "state.h"

#include <afe.h>
#include <bcom.h>
#include <stdlib.h>

struct vi
{
    int16_t v;
    int8_t i;
};

static void
swap_last_vi (struct vi *p)
{
    struct vi t;

    t = p[0]; p[0] = p[-1]; p[-1] = t;
}

static void
balance_cells (int count)
{
    struct vi viv[count];
    uint8_t indices[count];
    int16_t diff;
    int i, c;

    for (i = 0; i < count; i++) {
	viv[i].i = i;
	afe_get_voltage (NULL, i, &viv[i].v);
    }

    do {
	c = 0;

	for (i = 1; i < count; i++) {
	    if (viv[i].v > viv[i - 1].v) {
		swap_last_vi (viv + i);

		c = 1;
	    }
	}
    } while (c);

    diff = viv[0].v - viv[count - 1].v;

    for (c = 0; c < count - 1; c++) {
	if ((viv[c].v - viv[count - 1].v < prot_conf->cell_bal_diff) ||
	    (viv[c].v < prot_conf->cell_bal_min)) {
	    break;
	}
    }

    for (i = 0; i < c; i++) {
	indices[i] = viv[i].i;
    }

    afe_balance_cells (NULL, c, indices);
}

void update_balancing ()
{
    uint8_t count;

    afe_get_cell_count (NULL, &count);

    if (state_get () & BCOM_STATUS_BALANCING) {
	if (state_manage ()) {
	    balance_cells (count);
	}
    } else {
	afe_balance_cells (NULL, 0, NULL);
    }
}
