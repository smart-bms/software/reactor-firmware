/*
 * Copyright (c) 2021 Reactor Energy
 * Mieszko Mazurek <mimaz@gmx.com>
 */

#include "voltage.h"
#include "conf.h"

#include <afe.h>

static int cell_min;
static int cell_max;
static uint8_t allow_charging;
static uint8_t allow_discharging;

void
voltage_update ()
{
    uint8_t count;
    int16_t v, min_v, max_v;
    int i;

    afe_get_cell_count (NULL, &count);

    min_v = INT16_MAX;
    max_v = INT16_MIN;

    for (i = 0; i < count; i++) {
	afe_get_voltage (NULL, i, &v);

	if (v < min_v) {
	    min_v = v;
	}

	if (v > max_v) {
	    max_v = v;
	}
    }

    cell_min = min_v;
    cell_max = max_v;

    allow_charging = voltage_cell_level (voltage_cell_max ()) < 100;
    allow_discharging = voltage_cell_level (voltage_cell_min ()) > 0;
}

int
voltage_allow_charging ()
{
    return allow_charging;
}

int
voltage_allow_discharging ()
{
    return allow_discharging;
}

int
voltage_cell_min ()
{
    return cell_min;
}

int
voltage_cell_max ()
{
    return cell_max;
}

int
voltage_cell_level (int cell_voltage)
{
    return bcom_curve_compute (&bat_conf->cell_voltage_level, cell_voltage);
}
