/*
 * Copyright (c) 2020 Reactor Energy
 * Mieszko Mazurek <mimaz@gmx.com>
 */

#pragma once

#include <bcom.h>

struct bcom_port *com_init_uart (const char *uart_name);
