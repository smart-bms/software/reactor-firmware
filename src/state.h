/*
 * Copyright (c) 2021 Reactor Energy
 * Mieszko Mazurek <mimaz@gmx.com>
 */

#include <stdint.h>

void		state_update	    (void);
uint32_t	state_get	    (void);
uint32_t	state_set	    (uint32_t flags);
uint32_t	state_reset	    (uint32_t flags);
uint32_t	state_manual	    (void);
uint32_t	state_manage	    (void);
