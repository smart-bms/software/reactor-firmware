#include "led.h"

#include <drivers/gpio.h>

#define LED0_NODE	DT_ALIAS (led0)
#define LED0_DEVICE	DT_GPIO_LABEL (LED0_NODE, gpios)
#define LED0_PIN	DT_GPIO_PIN (LED0_NODE, gpios)
#define LED0_FLAGS	DT_GPIO_FLAGS (LED0_NODE, gpios)

static const struct device *gpio;

static void
init ()
{
    if (!gpio) {
	gpio = device_get_binding ("GPIOB");
	gpio_pin_configure (gpio, LED0_PIN, GPIO_OUTPUT_ACTIVE | LED0_FLAGS);
    }
}

void
led_on ()
{
    init ();
    gpio_pin_set (gpio, LED0_PIN, 1);
}

void
led_off ()
{
    init ();
    gpio_pin_set (gpio, LED0_PIN, 0);
}
