/*
 * Copyright (c) 2020 Reactor Energy
 * Mieszko Mazurek <mimaz@gmx.com>
 */

#pragma once

#include <stdint.h>

#define BQ769X0_OCD		    0x000001
#define BQ769X0_SCD		    0x000002
#define BQ769X0_OV		    0x000004
#define BQ769X0_UV		    0x000008
#define BQ769X0_OVRD_ALERT	    0x000010
#define BQ769X0_DEVICE_XREADY	    0x000020
#define BQ769X0_CC_READY	    0x000080

#define BQ769X0_SHUT_B		    0x000100
#define BQ769X0_SHUT_A		    0x000200
#define BQ769X0_TEMP_SEL	    0x000800
#define BQ769X0_ADC_EN		    0x001000
#define BQ769X0_LOAD_PRESENT	    0x008000

#define BQ769X0_CHG_ON		    0x010000
#define BQ769X0_DSG_ON		    0x020000
#define BQ769X0_CC_ONESHOT	    0x200000
#define BQ769X0_CC_EN		    0x400000
#define BQ769X0_DELAY_DIS	    0x800000

#define BQ769X0_SCD_THRESH_GET	    -1
#define BQ769X0_SCD_THRESH_22MV	    0
#define BQ769X0_SCD_THRESH_33MV	    1
#define BQ769X0_SCD_THRESH_44MV	    2
#define BQ769X0_SCD_THRESH_56MV	    3
#define BQ769X0_SCD_THRESH_67MV	    4
#define BQ769X0_SCD_THRESH_78MV	    5
#define BQ769X0_SCD_THRESH_89MV	    6
#define BQ769X0_SCD_THRESH_100MV    7

#define BQ769X0_SCD_DELAY_GET	    -1
#define BQ769X0_SCD_DELAY_70US	    0
#define BQ769X0_SCD_DELAY_100US	    1
#define BQ769X0_SCD_DELAY_200US	    2
#define BQ769X0_SCD_DELAY_400US	    3

#define BQ769X0_OCD_THRESH_GET	    -1
#define BQ769X0_OCD_THRESH_8MV	    0
#define BQ769X0_OCD_THRESH_11MV	    1
#define BQ769X0_OCD_THRESH_14MV	    2
#define BQ769X0_OCD_THRESH_17MV	    3
#define BQ769X0_OCD_THRESH_19MV	    4
#define BQ769X0_OCD_THRESH_22MV	    5
#define BQ769X0_OCD_THRESH_25MV	    6
#define BQ769X0_OCD_THRESH_28MV	    7
#define BQ769X0_OCD_THRESH_31MV	    8
#define BQ769X0_OCD_THRESH_33MV	    9
#define BQ769X0_OCD_THRESH_36MV	    10
#define BQ769X0_OCD_THRESH_39MV	    11
#define BQ769X0_OCD_THRESH_42MV	    12
#define BQ769X0_OCD_THRESH_44MV	    13
#define BQ769X0_OCD_THRESH_47MV	    14
#define BQ769X0_OCD_THRESH_50MV	    15

#define BQ769X0_OCD_DELAY_GET	    -1
#define BQ769X0_OCD_DELAY_8MS	    0
#define BQ769X0_OCD_DELAY_20MS	    1
#define BQ769X0_OCD_DELAY_40MS	    2
#define BQ769X0_OCD_DELAY_80MS	    3
#define BQ769X0_OCD_DELAY_160MS	    4
#define BQ769X0_OCD_DELAY_320MS	    5
#define BQ769X0_OCD_DELAY_640MS	    6
#define BQ769X0_OCD_DELAY_1280MS    7

#define BQ769X0_OV_DELAY_1S	    0
#define BQ769X0_OV_DELAY_2S	    1
#define BQ769X0_OV_DELAY_4S	    2
#define BQ769X0_OV_DELAY_8S	    3

#define BQ769X0_UV_DELAY_1S	    0
#define BQ769X0_UV_DELAY_4S	    1
#define BQ769X0_UV_DELAY_8S	    2
#define BQ769X0_UV_DELAY_16S	    3

int bq769x0_configure	    (const char *i2c_name, int address,
			     const char *gpio_name, int pin);
int bq769x0_cell_count	    ();
int bq769x0_voltage	    (int cellnum, uint32_t *value);
int bq769x0_current	    (int32_t *value);
int bq769x0_temperature	    (int32_t *value, int count);
int bq769x0_status	    (uint32_t *status);
int bq769x0_enable	    (uint32_t flags);
int bq769x0_disable	    (uint32_t flags);
int bq769x0_write	    (uint8_t addr, const uint8_t *data, uint8_t count);
int bq769x0_read	    (uint8_t addr, uint8_t *data, uint8_t count);
int bq769x0_set_delay	    (int scd, int ocd, int ov, int uv);
int bq769x0_set_scd	    (int thresh);
int bq769x0_get_scd	    (int *thresh);
int bq769x0_set_ocd	    (int thresh);
int bq769x0_get_ocd	    (int *thresh);
int bq769x0_set_ov	    (int value);
int bq769x0_get_ov	    (int *value);
int bq769x0_set_uv	    (int value);
int bq769x0_get_uv	    (int *value);
int bq769x0_set_balancing   (int flags);
int bq769x0_get_balancing   ();
