/*
 * Copyright (c) 2021 Reactor Energy
 * Mieszko Mazurek <mimaz@gmx.com>
 */

#include "conf.h"
#include "nvmem.h"

#include <string.h>

static struct bcom_prot_conf prot_conf_data = {
    .version = 1,
    .cell_ovp = 4250,
    .cell_uvp = 3150,
    .cell_bal_min = 3800,
    .cell_bal_diff = 15,
    .chg_ocp_hard = 200,
    .chg_ocp_soft = 100,
    .chg_ocp_soft_d = 1000,
    .dsg_ocp_hard = 500,
    .dsg_ocp_soft = 300,
    .dsg_ocp_soft_d = 5000,
    .chg_otp = 35,
    .chg_utp = 0,
    .dsg_otp = 35,
    .dsg_utp = 0,
};

static struct bcom_bat_conf bat_conf_data = {
    .version = 1,
    .nominal_capacity = 1700,
    .nominal_voltage = 3700,
};

const struct bcom_prot_conf *const prot_conf = &prot_conf_data;
const struct bcom_bat_conf *const bat_conf = &bat_conf_data;

void
conf_init ()
{
    bcom_curve_init_v (&bat_conf_data.cell_voltage_level,
		       3200, 4200, 2, 0, 100);
}

int
conf_use_prot (const struct bcom_prot_conf *conf)
{
    return 0;
}

int
conf_use_bat (const struct bcom_bat_conf *conf)
{
    return 0;
}

int
conf_write_prot ()
{
    return 0;
}

int
conf_write_bat ()
{
    return 0;
}

int
conf_load_prot ()
{
    return 0;
}

int
conf_load_bat ()
{
    return 0;
}
