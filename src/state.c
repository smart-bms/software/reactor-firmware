/*
 * Copyright (c) 2020,2021 Reactor Energy
 * Mieszko Mazurek <mimaz@gmx.com>
 */

#include "state.h"
#include "conf.h"
#include "current.h"
#include "temp.h"
#include "voltage.h"

#include <bcom.h>
#include <afe.h>

static uint32_t state = BCOM_STATUS_BALANCING;

K_MUTEX_DEFINE (state_lock);

static void
translate_flag (uint32_t *from, uint32_t *to, uint32_t from_flag, uint32_t to_flag)
{
    if (*from & from_flag) {
	*from &= ~from_flag;
	*to |= to_flag;
    }
}

static uint32_t
rcom_to_afe (uint32_t *rcom)
{
    uint32_t afe = 0;

    translate_flag (rcom, &afe, BCOM_STATUS_CHARGING, AFE_STAT_CHARGING);
    translate_flag (rcom, &afe, BCOM_STATUS_DISCHARGING, AFE_STAT_DISCHARGING);

    return afe;
}

static uint32_t
afe_to_rcom (uint32_t *afe)
{
    uint32_t rcom = 0;

    translate_flag (afe, &rcom, AFE_STAT_CHARGING, BCOM_STATUS_CHARGING);
    translate_flag (afe, &rcom, AFE_STAT_DISCHARGING, BCOM_STATUS_DISCHARGING);

    return rcom;
}

void
state_update ()
{
    uint32_t prot, charging, discharging;

    voltage_update ();
    temp_update ();
    current_update ();

    prot = afe_get_protection (NULL);

    if (state_manage ()) {
	charging = !0;
	charging &= !(prot & AFE_PROT_CELL_OVP);
	charging &= !(prot & AFE_PROT_CHG_OCP);
	charging &= !(prot & AFE_PROT_OTP);
	charging &= !(prot & AFE_PROT_UTP);
	charging &= !!voltage_allow_charging ();
	charging &= !!temp_allow_charging ();
	charging &= !!current_allow_charging ();

	if (charging) {
	    afe_enable (NULL, AFE_STAT_CHARGING);
	} else {
	    afe_disable (NULL, AFE_STAT_CHARGING);
	}

	discharging = !0;
	discharging &= !(prot & AFE_PROT_CELL_UVP);
	discharging &= !(prot & AFE_PROT_DSG_OCP);
	discharging &= !(prot & AFE_PROT_OTP);
	discharging &= !(prot & AFE_PROT_UTP);
	discharging &= !!voltage_allow_discharging ();
	discharging &= !!temp_allow_discharging ();
	discharging &= !!current_allow_discharging ();

	if (discharging) {
	    afe_enable (NULL, AFE_STAT_DISCHARGING);
	} else {
	    afe_disable (NULL, AFE_STAT_DISCHARGING);
	}
    }
}

uint32_t
state_get ()
{
    uint32_t afe;

    afe_get_status (0, &state);

    return state | afe_to_rcom (&afe);
}

uint32_t
state_set (uint32_t flags)
{
    afe_enable (NULL, rcom_to_afe (&flags));

    k_mutex_lock (&state_lock, K_FOREVER);
    state |= flags;
    k_mutex_unlock (&state_lock);

    return state_get ();
}

uint32_t
state_reset (uint32_t flags)
{
    afe_disable (NULL, rcom_to_afe (&flags));

    k_mutex_lock (&state_lock, K_FOREVER);
    state &= ~flags;
    k_mutex_unlock (&state_lock);

    return state_get ();
}

uint32_t
state_manual ()
{
    return state_get () & BCOM_STATUS_MANUAL;
}

uint32_t
state_manage ()
{
    return !state_manual (); 
}
