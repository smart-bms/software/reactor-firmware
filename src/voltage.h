/*
 * Copyright (c) 2021 Reactor Energy
 * Mieszko Mazurek <mimaz@gmx.com>
 */

#pragma once

void	voltage_update		    (void);
int	voltage_allow_charging	    (void);
int	voltage_allow_discharging   (void);
int	voltage_cell_min	    (void);
int	voltage_cell_max	    (void);
int	voltage_cell_level	    (int cell_voltage);
