/*
 * Copyright (c) 2020,2021 Reactor Energy
 * Mieszko Mazurek <mimaz@gmx.com>
 */

#include <afe.h>
#include <com.h>
#include <zephyr.h>
#include <bcom.h>
#include <conf.h>
#include <drivers/uart.h>
#include <string.h>

#include "bal.h"
#include "state.h"
#include "bq769x0.h"
#include "nvmem.h"
#include "led.h"

void main (void)
{
    const struct device *uart;

    bcom_port_bind (com_init_uart ("UART_3"));

    uart = device_get_binding ("UART_3");

    while (!uart) {
	led_on ();
	k_msleep (50);
	led_off ();
	k_msleep (50);
    }

    afe_by_name (&afe_default, "fake");

    if (!afe_default) {
	while (1) {
	    bcom_message ("couldn't set afe");
	    k_msleep (1000);
	}
    }

    afe_activate (NULL);

    while (1) {
	led_on ();
	k_msleep (10);
	led_off ();

	/*bcom_message ("hello, world!");*/
	k_msleep (990);
    }
}
