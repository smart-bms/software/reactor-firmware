/*
 * Copyright (c) 2021 Reactor Energy
 * Mieszko Mazurek <mimaz@gmx.com>
 */

#include "temp.h"
#include "conf.h"

#include <afe.h>
#include <bcom.h>

static uint8_t allow_charging;
static uint8_t allow_discharging;

void
temp_update ()
{
    int16_t values[8], min, max;
    size_t count, i;

    count = 8;
    afe_get_temperatures (NULL, values, &count);
    min = INT16_MAX;
    max = INT16_MIN;

    for (i = 0; i < count; i++) {
	if (min > values[i]) {
	    min = values[i];
	}

	if (max < values[i]) {
	    max = values[i];
	}
    }

    allow_charging = max <= prot_conf->chg_otp * 10 &&
		     min >= prot_conf->chg_utp * 10;

    allow_discharging = max <= prot_conf->dsg_otp * 10 &&
			min >= prot_conf->dsg_utp * 10;
}

int
temp_allow_charging ()
{
    return allow_charging;
}

int
temp_allow_discharging ()
{
    return allow_discharging;
}
