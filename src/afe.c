/*
 * Copyright (c) 2020 Reactor Energy
 * Mieszko Mazurek <mimaz@gmx.com>
 */

#include "afe.h"

#include <bcom.h>

#include <string.h>
#include <kernel.h>

struct afe *afe_default;

static int
afe_by_name_iter (struct afe *afe, void *user_data)
{
    void **closure = user_data;

    if (strcmp (afe->name, closure[1]) == 0) {
	*((struct afe **) closure[0]) = afe;
    }

    return 1;
}

int
afe_by_name (struct afe **afe, const char *name)
{
    void *closure[] = { afe, (void *) name };

    if (afe_iter (afe_by_name_iter, closure)) {
	return 0;
    }

    return -1;
}

int
afe_iter (afe_iter_t iter, void *user_data)
{
    int res;

    STRUCT_SECTION_FOREACH (afe, it) {
	res = iter (it, user_data);

	if (!res) {
	    return res;
	}
    }

    return 0;
}

#define APICALL(afe, name, ...) \
    do { \
	if (!afe) { \
	    afe = afe_default; \
	} \
	if (afe && afe->api && afe->api->name) { \
	    return afe->api->name (afe, ##__VA_ARGS__); \
	} else { \
	    return -1; \
	} \
    } while (0)

int
afe_activate (struct afe *afe)
{
    APICALL (afe, activate);
}

int
afe_deactivate (struct afe *afe)
{
    APICALL (afe, deactivate);
}

int
afe_get_cell_count (struct afe *afe, uint8_t *count)
{
    APICALL (afe, get_cell_count, count);
}

int
afe_get_voltage (struct afe *afe, int16_t cellnum, int16_t *voltage)
{
    APICALL (afe, get_voltage, cellnum, voltage);
}

int
afe_get_current (struct afe *afe)
{
    APICALL (afe, get_current);
}

int
afe_get_protection (struct afe *afe)
{
    APICALL (afe, get_protection);
}

int
afe_get_range (struct afe *afe, int what, int *low, int *high)
{
    APICALL (afe, get_range, what, low, high);
}

int
afe_get_temperatures (struct afe *afe, int16_t *values, size_t *count)
{
    APICALL (afe, get_temperatures, values, count);
}

int
afe_get_status (struct afe *afe, uint32_t *status)
{
    APICALL (afe, get_status, status);
}

int
afe_enable (struct afe *afe, int status)
{
    APICALL (afe, enable, status);
}

int
afe_disable (struct afe *afe, int status)
{
    APICALL (afe, disable, status);
}

int
afe_get_error (struct afe *afe, uint32_t *error)
{
    APICALL (afe, get_error, error);
}

int
afe_read_registers (struct afe *afe, int addr, void *data, int bytes)
{
    APICALL (afe, read_registers, addr, data, bytes);
}

int
afe_write_registers (struct afe *afe, int addr, const void *data, int bytes)
{
    APICALL (afe, write_registers, addr, data, bytes);
}

int
afe_get_balancing (struct afe *afe)
{
    APICALL (afe, get_balancing);
}

int
afe_balance_cells (struct afe *afe, uint8_t count, const uint8_t *indices)
{
    APICALL (afe, balance_cells, count, indices);
}
