/*
 * Copyright (c) 2020 Reactor Energy
 * Mieszko Mazurek <mimaz@gmx.com>
 */

/*
 * generic bms analog frontend interface
 */

#pragma once

#include <stdint.h>
#include <stddef.h>

#define AFE_PROT_CELL_OVP	    1
#define AFE_PROT_CELL_UVP	    2
#define AFE_PROT_CHG_OCP	    4
#define AFE_PROT_DSG_OCP	    8
#define AFE_PROT_OTP		    16
#define AFE_PROT_UTP		    32

#define AFE_STAT_CHARGING	    1
#define AFE_STAT_DISCHARGING	    2
#define AFE_STAT_ACTIVE		    4
#define AFE_STAT_PROTECTED	    8
#define AFE_STAT_ERROR		    16
#define AFE_STAT_CURRENT	    32
#define AFE_STAT_VOLTAGE	    64
#define AFE_STAT_TEMPERATURE	    128

#define AFE_ERROR_NONE		    0
#define AFE_ERROR_COMMUNICATION	    1
#define AFE_ERROR_INTERNAL	    2
#define AFE_ERROR_UNKNOWN	    256

struct afe_api;

struct afe
{
    struct afe_api *api;
    const char	   *name;
    void	   *data;
};

struct afe_api
{
    int (*activate)	    (struct afe    *afe);
    int (*deactivate)	    (struct afe    *afe);
    int (*get_cell_count)   (struct afe    *afe,
			     uint8_t	   *count);
    int (*get_voltage)	    (struct afe	   *afe,
			     int16_t	    cellnum,
			     int16_t	   *voltage);
    int (*get_current)	    (struct afe    *afe);
    int (*get_protection)   (struct afe    *afe);
    int (*get_range)	    (struct afe    *afe,
			     int	    what,
			     int	   *low,
			     int	   *high);
    int (*get_temperatures) (struct afe    *afe,
			     int16_t	   *values,
			     size_t	   *count);
    int (*get_status)	    (struct afe    *afe,
			     uint32_t	   *status);
    int (*enable)	    (struct afe    *afe,
			     int	    status);
    int (*disable)	    (struct afe    *afe,
			     int	    status);
    int (*get_error)	    (struct afe	   *afe,
			     uint32_t	   *error);
    int (*read_registers)   (struct afe    *afe,
			     int	    addr,
			     void	   *data,
			     int	    bytes);
    int (*write_registers)  (struct afe    *afe,
			     int	    addr,
			     const void	   *data,
			     int	    bytes);
    int (*get_balancing)    (struct afe    *afe);
    int	(*balance_cells)    (struct afe	   *afe,
			     uint8_t	    count,
			     const uint8_t *indices);
};

typedef int (*afe_iter_t) (struct afe *, void *);

extern struct afe *afe_default;

int afe_by_name		    (struct afe	  **afe,
			     const char	   *name);
int afe_iter		    (afe_iter_t	    iter,
			     void	   *user_data);
int afe_activate	    (struct afe    *afe);
int afe_deactivate	    (struct afe    *afe);
int afe_get_cell_count	    (struct afe	   *afe,
			     uint8_t	   *count);
int afe_get_voltage	    (struct afe	   *afe,
			     int16_t	    cellnum,
			     int16_t	   *voltage);
int afe_get_current	    (struct afe	   *afe);
int afe_get_protection	    (struct afe	   *afe);
int afe_get_range	    (struct afe	   *afe,
			     int	    what,
			     int	   *low,
			     int	   *high);
int afe_get_temperatures    (struct afe	   *afe,
			     int16_t	   *values,
			     size_t	   *count);
int afe_get_status	    (struct afe	   *afe,
			     uint32_t	   *status);
int afe_enable		    (struct afe	   *afe,
			     int	    status);
int afe_disable		    (struct afe	   *afe,
			     int	    status);
int afe_get_error	    (struct afe	   *afe,
			     uint32_t	   *error);
int afe_read_registers	    (struct afe	   *afe,
			     int	    addr,
			     void	   *data,
			     int	    bytes);
int afe_write_registers	    (struct afe	   *afe,
			     int	    addr,
			     const void	   *data,
			     int	    bytes);
int afe_get_balancing	    (struct afe	   *afe);
int afe_balance_cells	    (struct afe	   *afe,
			     uint8_t	    count,
			     const uint8_t *indices);

#define AFE_DEFINE(afename, afeapi, afedata) \
    STRUCT_SECTION_ITERABLE (afe, afe_##afename) = { \
	.api = &afeapi, \
	.name = #afename, \
	.data = afedata, \
    };
