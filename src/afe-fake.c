/*
 * Copyright (c) 2020 Reactor Energy
 * Mieszko Mazurek <mimaz@gmx.com>
 */

#include "afe.h"
#include "bq769x0.h"

#include <stdlib.h>
#include <string.h>
#include <zephyr.h>

static int fake_prot;
static int fake_stat;
static int balancing;
static uint8_t registers[16];

static int
activate (struct afe *afe)
{
    fake_prot = 0;
    fake_stat = AFE_STAT_ACTIVE;

    return 0;
}

static int
deactivate (struct afe *afe)
{
    fake_stat = 0;
    return 0;
}

static int
get_cell_count (struct afe *afe, uint8_t *count)
{
    *count = 10;
    return 0;
}

static uint32_t
rand_uint ()
{
    static uint64_t seed;

    return seed = (seed * 0x5deece66dull + 0xbull) & ((1ull << 48) - 1);
}

static int
get_voltage (struct afe *afe, int16_t cellnum, int16_t *voltage)
{
    int16_t sum, cellv, i;
    uint8_t count;

    if (afe_get_cell_count (afe, &count)) {
	return -1;
    }

    if (cellnum < 0) {
	sum = 0;
	
	for (i = 0; i < count; i++) {
	    if (get_voltage (afe, i, &cellv)) {
		return -1;
	    }
	}

	*voltage = sum;
    } else if (cellnum < count) {
	*voltage = 3950 + cellnum % 3 * 5 + rand_uint () % 20;
    } else {
	*voltage = 0;
    }

    return 0;
}

static int	
get_current (struct afe *afe)
{
    return -1000;
}

static int
get_protection (struct afe *afe)
{
    return fake_prot;
}

static int
get_range (struct afe *afe, int what, int *low, int *high)
{
    return -1;
}

static int
get_temperatures (struct afe *afe, int16_t *values, size_t *count)
{
    uint8_t i;

    if (*count > 3) {
	*count = 3;
    }

    for(i = 0; i < *count; i++) {	
	values[i] = rand_uint () % 50 + 100;;
    }	

    return 0;
}

static int
get_status (struct afe *afe, uint32_t *status)
{
    *status = 0;

    return 0;
}

static int
enable (struct afe *afe, int status)
{
    fake_stat |= status;

    return 0;
}

static int
disable (struct afe *afe, int status)
{
    fake_stat &= status;

    return 0;
}

static int
get_error (struct afe *afe, uint32_t *error)
{
    uint32_t status;

    if (get_status (afe, &status)) {
	return -1;
    }

    if (status & AFE_STAT_ERROR) {
	return AFE_ERROR_UNKNOWN;
    }

    return AFE_ERROR_NONE;
}

static int
read_registers (struct afe *afe, int addr, void *data, int bytes)
{
    int count;

    count = sizeof (registers) - addr;

    if (count > bytes) {
	count = bytes;
    }

    if (count < 0) {
	count = 0;
    }

    if (count > 0) {
	memcpy (data, registers + addr, count);
    }

    return count;
}

static int
write_registers (struct afe *afe, int addr, const void *data, int bytes)
{
    int count;

    count = sizeof (registers) - addr;

    if (count > bytes) {
	count = bytes;
    }

    if (count > 0) {
	memcpy (registers + addr, data, count);
    }

    return count;
}

static int
get_balancing (struct afe *afe)
{
    return balancing;
}

static int
balance_cells (struct afe *afe, uint8_t count, const uint8_t *indices)
{
    int i;

    balancing = 0;

    for (i = 0; i < count; i++) {
	if (indices[i] < 10) {
	    balancing |= 1 << indices[i];
	}
    }

    return 0;
}

static struct afe_api api = {
    .activate = activate,
    .deactivate = deactivate,
    .get_cell_count = get_cell_count,
    .get_voltage = get_voltage,
    .get_current = get_current,
    .get_protection = get_protection,
    .get_range = get_range,
    .get_temperatures = get_temperatures,
    .get_status = get_status,
    .enable = enable,
    .disable = disable,
    .get_error = get_error,
    .read_registers = read_registers,
    .write_registers = write_registers,
    .get_balancing = get_balancing,
    .balance_cells = balance_cells,
};

AFE_DEFINE (fake, api, 0);
